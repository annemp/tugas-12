@extends('layout.master')

@section('judul')
List Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary my-3"> Tambah Cast </a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
       
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key=> $item)
        <tr>
            <td>{{$key + 1}} </td>
            <td>{{$item->nama}} </td>
            <td> 
                
                <form action="/cast/{{$item->id}} " method="POST">
                    @csrf
                    @method('delete')
                    
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
      @empty
          
      @endforelse
        
    </tbody>
  </table>
@endsection
