@extends('layout.master')

@section('judul')
Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Pemain Film</label>
      <input type="text" value="{{$cast->nama}}" name="nama" > 
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
    <div class="form-group">
      <label>Umur</label>
      <input type="umur" class="form-control" name="umur">
    </div> 
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
    <div class="form-group">
        <label>Bio</label>
      <textarea name="bio" class="form-control">{{$cast->bio}} </textarea>
      </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection