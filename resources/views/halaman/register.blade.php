@extends('layout.master')

@section('judul')
Register
@endsection

@section('content')
    
        <form action="/welcome" method="post">
            @csrf
        <label> First Name : </label><br>
        <input type="text" name="fname"><br><br>
        <label> Last Name : </label><br>
        <input type="text" name="lname"><br><br>

        <label> Gender </label><br>
        <input type="radio"> Male <br>
        <input type="radio"> Female <br><br>
        <label> Nationality </label><br><br> 
        <select name="Nationality"> <br>
            <option value="Indonesia"> Indonesia </option>
            <option value="India"> India </option>
            <option value="Malaysia"> Malaysia </option>
        </select><br><br>

        <label> Language Spoken </label><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>
        <label> Bio </label><br><br>

        <textarea name="message" rows="10" cols="30"> </textarea><br>
               
        <input type="submit" value="Send">

@endsection