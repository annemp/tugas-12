<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.register');
    }    
    public function send(Request $request)
    {
        //dd($request->all());
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('halaman.welcome', compact('namaDepan', 'namaBelakang'));
    }  
}
