<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = Cast::all();
        return view('cast.index',compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Cast.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=> 'required',
            'umur'=> 'required',
            'bio' => 'required',
        ]);

        $cast = new Cast;
        $cast -> nama = $request -> nama;
        $cast -> umur = $request -> umur;
        $cast -> bio = $request -> bio;
        $cast-> save();

        return redirect('/cast/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = cast::find($id);
        return view('cast.show', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = cast::find($id);
        return view('cast.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'=> 'required',
            'umur'=> 'required',
            'bio' => 'required',
        ],
        [
            'nama.required'=> 'Inputan nama harus diisi',
            'umur.required'=> 'Inputan umur harus diisi',
            'bio.required'=> 'Inputan bio harus diisi',
        ]
        
        );

        $cast = Cast::find($id);
        $cast -> nama = $request -> nama;
        $cast -> umur = $request -> umur;
        $cast -> bio = $request -> bio;
        $cast-> save();

        return redirect('/cast/create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast -> delete();
        return redirect('/cast');
    }
}
